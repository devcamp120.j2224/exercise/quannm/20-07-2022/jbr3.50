package com.devcamp.jbr350.jbr350;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr350Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr350Application.class, args);
	}

}
