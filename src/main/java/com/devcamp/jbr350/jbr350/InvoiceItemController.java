package com.devcamp.jbr350.jbr350;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InvoiceItemController {
    @CrossOrigin
    @GetMapping("/invoices")
    public ArrayList<InvoiceItem> getListInvoice() {
        ArrayList<InvoiceItem> list = new ArrayList<InvoiceItem>();

        InvoiceItem invoiceItem1 = new InvoiceItem("HD01", "TV", 2, 300);
        InvoiceItem invoiceItem2 = new InvoiceItem("HD02", "Motor", 1, 400);
        InvoiceItem invoiceItem3 = new InvoiceItem("HD03", "Sofa", 3, 55);

        list.add(invoiceItem1);
        list.add(invoiceItem2);
        list.add(invoiceItem3);

        return list;
    }

    // public static void main(String[] args) {
    //     InvoiceItem invoiceItem1 = new InvoiceItem("HD01", "TV", 2, 300);
    //     InvoiceItem invoiceItem2 = new InvoiceItem("HD02", "Motor", 1, 400);
    //     System.out.println(invoiceItem1 + "," + invoiceItem2);
    // }
    
}
